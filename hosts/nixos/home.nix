{ pkgs, ...}:
{
  imports = [
    ../common.nix
    ../../programs/i3.nix
    ../../programs/picom.nix
    ../../programs/firefox.nix
  ];
  home.username = "kyle";
  home.homeDirectory = "/home/kyle";
  home.packages = with pkgs; [
    i3status-rust
    pavucontrol
    picom
    feh
  ];
}
