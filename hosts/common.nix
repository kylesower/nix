{ pkgs, inputs, config, lib, vars, ... }:

{
  imports = [
    ../programs/alacritty.nix
    # For some reason firefox 123 isn't on Darwin yet
    # ../programs/firefox.nix
  ];
  # DO NOT CHANGE
  home.stateVersion = "23.11";

  home.packages = with pkgs; [
    fish
    neovim
    alacritty
    tmux
    yazi
    ripgrep
    fzf
    babelfish
    (nerdfonts.override { fonts = [ "DejaVuSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')
  ];

  /*
  home.file = {
    ".config/fish" = {
      source = ../programs/fish;
      recursive = true;
    };
    ".config/nvim" = {
      source = ../programs/nvim;
      recursive = true;
    };
    ".tmux.conf".source = ../.tmux.conf;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
  };
  */
  home.activation.linkMyStuff = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
    ln -sf $HOME/nix/programs/nvim ${config.home.homeDirectory}/.config 
    ln -sf $HOME/nix/programs/fish ${config.home.homeDirectory}/.config
    ln -sf $HOME/nix/programs/.tmux.conf ${config.home.homeDirectory}
  '';

  # Home Manager can also manage your environment variables through
  # 'home.sessionVariables'. If you don't want to manage your shell through Home
  # Manager then you have to manually source 'hm-session-vars.sh' located at
  # either
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  ~/.local/state/nix/profiles/profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/kyle/etc/profile.d/hm-session-vars.sh
  #
  /*
  home.sessionVariables = {
    EDITOR = vars.editor;
  };
  */

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  # programs.fish.enable = true;

  programs.git = {
    enable = true;
    userName = "kylesower";
    userEmail = "gitlab@leastaction.me";
    extraConfig = {
      init = {
        defaultBranch = "master";
      };
    };
  };
}
