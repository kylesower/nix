{ config, pkgs, lib, vars, inputs, ...}:
let
  homeDir = "/Users/kyle";
in {
  imports = [
    ../common.nix
  ];
  home.username = "kyle";
  home.homeDirectory = lib.mkForce (homeDir);

  programs.alacritty.settings.window = {
    dimensions = {
      columns = 175;
      lines = 60;
    };
    option_as_alt = "Both";
  };
  home.activation.linkMacStuff = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
    ln -sf $HOME/nix/programs/amethyst ${homeDir}/.config
  '';
}
