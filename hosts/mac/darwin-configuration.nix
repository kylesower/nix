{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    vim
  ];

  security.pam.enableSudoTouchIdAuth = true;
  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;
  nix.package = pkgs.nix;

  nixpkgs.config.allowUnfree = true;

  # Necessary for using flakes on this system.
  nix.settings.experimental-features = "nix-command flakes";

  homebrew.enable = true;
  homebrew.brews = [
  ];
  homebrew.casks = [
    "amethyst"
  ];

  # Create /etc/zshrc that loads the nix-darwin environment.
  programs.fish.enable = true;  # default shell on catalina

  # Set Git commit hash for darwin-version.
  # system.configurationRevision = self.rev or self.dirtyRev or null;

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;

  # The platform the configuration will be used on.
  # nixpkgs.hostPlatform = "aarch64-darwin";

  networking.hostName = "Kyle";
}
