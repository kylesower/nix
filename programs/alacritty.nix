{ ... }:

{
  programs.alacritty = {
    enable = true;
    settings = {

      import = [ "~/config/alacritty/themes/themes/nightfox.toml" ];
      env = {
        TERM = "alacritty";
      };
      font = {
        size = 16.0;
        offset = {
          x = 0;
          y = 3;
        };
        normal = {
          family = "DejaVuSansM Nerd Font Mono";
          style = "Regular";
        };
      };
      shell = {
        program = "/run/current-system/sw/bin/fish";
        args = [ "--login" ];
      };
      keyboard = {
        bindings = [
          {
            key = "Space";
            mods = "Shift";
            chars = "\\u005c\\u0008 ";
          }
        ];
      };
    };
  };
}
