{ config, lib, pkgs, ... }:

let 
  mod = "Mod4";
in {
  xsession.windowManager.i3 = {
    enable = true;
    config = {
      modifier = mod;
      fonts = {
        names = [ "DejaVu Sans Mono" ];
        size = 16.0;
      };

      keybindings = {
        "${mod}+d" = "exec ${pkgs.dmenu}/bin/dmenu_run";
        "${mod}+Return" = "exec alacritty";
        "${mod}+Shift+q" = "kill";
        "${mod}+j" = "= focus left";
        "${mod}+k" = "focus down";
        "${mod}+l" = "focus up";
        "${mod}+semicolon" = "focus right";
        "${mod}+Left" = "focus left";
        "${mod}+Down" = "focus down";
        "${mod}+Up" = "focus up";
        "${mod}+Right" = "focus right";
        "${mod}+Shift+j" = "move left";
        "${mod}+Shift+k" = "move down";
        "${mod}+Shift+l" = "move up";
        "${mod}+Shift+semicolon" = "move right";
        "${mod}+Shift+Left" = "move left";
        "${mod}+Shift+Down" = "move down";
        "${mod}+Shift+Up" = "move up";
        "${mod}+Shift+Right" = "move right";
        "${mod}+h" = "split h";
        "${mod}+v" = "split v";
        "${mod}+f" = "fullscreen toggle";
        "${mod}+s" = "layout stacking";
        "${mod}+w" = "layout tabbed";
        "${mod}+e" = "layout toggle split";
        "${mod}+Shift+space" = "floating toggle";
        "${mod}+space" = "focus mode_toggle";
        "${mod}+a" = "focus parent";
        "${mod}+1" = "workspace number 1";
        "${mod}+2" = "workspace number 2";
        "${mod}+3" = "workspace number 3";
        "${mod}+4" = "workspace number 4";
        "${mod}+5" = "workspace number 5";
        "${mod}+6" = "workspace number 6";
        "${mod}+7" = "workspace number 7";
        "${mod}+8" = "workspace number 8";
        "${mod}+9" = "workspace number 9";
        "${mod}+0" = "workspace number 10";
        "${mod}+Mod1+1" = "move container to workspace number 1";
        "${mod}+Mod1+2" = "move container to workspace number 2";
        "${mod}+Mod1+3" = "move container to workspace number 3";
        "${mod}+Mod1+4" = "move container to workspace number 4";
        "${mod}+Mod1+5" = "move container to workspace number 5";
        "${mod}+Mod1+6" = "move container to workspace number 6";
        "${mod}+Mod1+7" = "move container to workspace number 7";
        "${mod}+Mod1+8" = "move container to workspace number 8";
        "${mod}+Mod1+9" = "move container to workspace number 9";
        "${mod}+Mod1+0" = "move container to workspace number 10";
        "${mod}+Shift+r" = "restart";
        "${mod}+Shift+e" = "exec \"i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'\"";
      };

      startup = [
        {
          command = "exec --no-startup-id xss-lock --transfer-sleep-lock --i3lock --nofork";
          always = true;
          notification = false;
        }
        {
          command = "exec --no-startup-id nm-applet";
          always = true;
          notification = false;
        }
        {
          command = "exec feh --bg-max --randomize ~/bg/* &";
          always = true;
          notification = false;
        }
      ];
      bars = [
        {
          fonts = {
            names = [ "DejaVu Sans Mono" ];
            size = 16.0;
          };
          position = "bottom";
          statusCommand = "${pkgs.i3status-rust}/bin/i3status-rs ~/.config/i3status-rust/config-options.toml";
        }
      ];
    };
  };

  # Status bar
  programs.i3status-rust = {
    enable = true;
    bars = {
      options = {
        icons = "awesome4";
        theme = "gruvbox-dark";
        blocks = [
          {
            block = "net";
            device = "wlp3s0f0u3";
            interval = 5;
            format = " $icon {$ssid $ip $signal_strength} via $device $graph_down $graph_up ";
          }
          {
            block = "disk_space";
            path = "/";
            info_type = "available";
            alert_unit = "GB";
            interval = 20;
            warning = 20.0;
            alert = 10.0;
            format = " $icon $available ";
          }
          {
            block = "memory";
            format = " $icon $mem_total_used_percents.eng(w:2) ";
          }
          {
            block = "cpu";
            info_cpu = 20;
            warning_cpu = 50;
            critical_cpu = 90;
            format = " $icon $utilization $barchart ";
          }
          {
            block = "sound";
            click = [
              {
                button = "left";
                cmd = "pavucontrol";
              }
            ];
          }
          {
            block = "time";
            interval = 1;
            format = " $timestamp.datetime(f:'%Y-%m-%d %H:%M:%S') ";
          }
        ];
      };
    };
  };
}
