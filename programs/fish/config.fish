if status is-interactive
    # Commands to run in interactive sessions can go here
end

# fish_add_path $HOME/.cargo/bin
# fish_add_path $HOME/easy_alias
# fish_add_path $HOME/bible
fish_add_path /opt/homebrew/bin
# fish_add_path /run/current-system/sw/bin

# set -gx NIX_PATH nixos-config=/Users/Kyle/.nixpkgs/darwin-configuration.nix:/Users/kyle/.nix-defexpr/channels:darwin-config=/Users/kyle/.nixpkgs/darwin-configuration.nix:/nix/var/nix/profiles/per-user/root/channels
if test "$(hostname)" = "YN6MQQRX1T"
    set -gx VIMRUNTIME ~/nvim-macos/share/nvim/runtime
    fish_add_path ~/nvim-macos/bin/
    source /Users/KSower/.docker/init-fish.sh || true # Added by Docker Desktop
    # source /Users/KSower/.cargo/env || true # Added by Docker Desktop
    dev_setup
end

if test "$(hostname)" = "nixos"
    # fish_add_path ~/dev/easy_alias/target/release/
    # fish_add_path ~/dev/bible/target/release/
    # cat ~/.nix-profile/etc/profile.d/hm-session-vars.sh | babelfish | source
    cat /etc/profiles/per-user/kyle/etc/profile.d/hm-session-vars.sh | babelfish | source
end

# set -g fish_greeting (bible)

# Now `...` Will go up two directories, etc.
function multicd
    echo cd (string repeat -n (math (string length -- $argv[1]) - 1) ../)
end
abbr --add dotdot --regex '^\.\.+$' --function multicd

abbr -a nv nvim .
abbr -a nvconf nvim ~/.config/nvim -c '"cd ~/.config/nvim"'
abbr -a sv source venv/bin/activate.fish
abbr -a dea deactivate
abbr -a ro robot --flattenkeywords tag:Flatten --loglevel DEBUG:INFO
abbr -a al aws sso login
abbr -a aoc "cargo build --release && ./target/release/aoc"
abbr -a ta tmux attach
abbr -a tk tmux kill-server

function fish_user_key_bindings
    # Execute this once per mode that emacs bindings should be used in
    fish_default_key_bindings -M insert

    # Then execute the vi-bindings so they take precedence when there's a conflict.
    # Without --no-erase fish_vi_key_bindings will default to
    # resetting all bindings.
    # The argument specifies the initial mode (insert, "default" or visual).
    fish_vi_key_bindings --no-erase insert
end


if status is-interactive
    # Commands to run in interactive sessions can go here
end
