function reset_spaces
    while test -n "$(yabai -m query --spaces | jq '.[] | select(.windows==[]).index')";
        set spaces (yabai -m query --spaces | jq '.[] | select(.windows==[]).index')
        yabai -m space --destroy $spaces[1]
    end;
end
