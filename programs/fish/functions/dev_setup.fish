function make_session -a name dir
    if not tmux has-session -t $name &> /dev/null
        tmux new-session -s $name -n $name -d
        tmux send-keys -t $name:0 "cd $dir" Enter
        tmux new-window -t $name:1 -n nvim "cd $dir; nvim ."
    else
        echo "Session already exists for $name"
    end
end
function dev_setup
    if test "$TMUX" = ""
        make_session api-crit-func ~/Projects/api-critical-functionality/
        make_session qacentral ~/Projects/qacentral/
        make_session am-testing ~/Projects/notnetsuite-automated-testing/
        make_session libraries ~/Projects/libraries/
    end
end
