function fish_prompt
    printf '%s' (set_color green) (hostname) (set_color white) : (set_color brred) [ (set_color brcyan) $USER (set_color brred) ] (set_color white) : (set_color purple) (string replace $HOME "~" (pwd))
    if test (git branch --show-current 2>/dev/null)
        set_color white
        printf ' %s(%s%s%s)' (set_color red) (set_color blue) (git branch --show-current) (set_color red)
    end
    set_color yellow
    printf '> '
    set_color normal
end
