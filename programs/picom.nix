{ config, lib, pkgs, ... }:

{
  services.picom = {
    enable = true;
    shadowExclude = [
      "name = 'Notification'"
      "class_g = 'Conky'"
      "class_g ?= 'Notify-osd'"
      "class_g = 'Cairo-clock'"
      "_GTK_FRAME_EXTENTS@:c"
    ];
    inactiveOpacity = 0.9;
    activeOpacity = 0.95;
    # inactiveOpacityOverride = false;
    # focusExclude = [ "class_g = 'Cairo-clock'" ];
    wintypes = {
      tooltip = {
        fade = true;
        shadow = true;
        opacity = 0.75;
        focus = true;
        fullShadow = false;
      };
      dock = { 
        shadow = false;
        clipShadowAbove = true;
      };
      dnd = { shadow = false; };
      popup_menu = { opacity = 0.8; };
      dropdown_menu = { opacity = 0.8; };
    };
    settings = {
      blur-background-exclude = [
        "window_type = 'dock'"
        "window_type = 'desktop'"
        "_GTK_FRAME_EXTENTS@:c"
      ];
    };
  };
}
