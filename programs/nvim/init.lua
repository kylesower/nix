vim.g.mapleader = "l"

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    "ThePrimeagen/harpoon",
    "tpope/vim-fugitive",
    'tpope/vim-sleuth',
    {
        "folke/zen-mode.nvim",
        opts = {
            plugins = {
                alacritty = {
                    enabled = true,
                    font = "18",

                }
            }
        }
    },
    { import = "plugins" },

    -- [Themes]
    { "craftzdog/solarized-osaka.nvim", priority = 100, enabled = false},
    { "EdenEast/nightfox.nvim", priority = 100, enabled = true},

    {
        "rebelot/kanagawa.nvim",
        enabled = false,
        priority = 100,
        opts = {
            compile = false,  -- enable compiling the colorscheme
            undercurl = true, -- enable undercurls
            commentStyle = { italic = false },
            functionStyle = {},
            keywordStyle = { italic = true },
            statementStyle = { bold = true },
            typeStyle = {},
            transparent = false,   -- do not set background color
            dimInactive = false,   -- dim inactive window `:h hl-NormalNC`
            terminalColors = true, -- define vim.g.terminal_color_{0,17}
            colors = {             -- add/modify theme and palette colors
                palette = {},
                theme = { wave = {}, lotus = {}, dragon = {}, all = { ui = { bg_gutter = "none" } } },
            },
            overrides = function(colors) -- add/modify highlights
                return {}
            end,
            theme = "wave",    -- Load "wave" theme when 'background' option is not set
            background = {     -- map the value of 'background' option to a theme
                dark = "wave", -- try "dragon" !
                light = "lotus"
            },
        }
    },
})
-- vim.cmd("colorscheme kanagawa")
vim.cmd("colorscheme nightfox")
-- vim.cmd("colorscheme solarized-osaka")
require("ks.remap")
require("ks.set")
require("ks.robot")
