return {
    -- "gc" to comment visual regions/lines
    { 'numToStr/Comment.nvim',    event = "VeryLazy",                         opts = {} },
    -- Highlight TODOs and stuff
    { 'folke/todo-comments.nvim', dependencies = { 'nvim-lua/plenary.nvim' }, opts = { signs = false } },
}
