return {
    {
        "neovim/nvim-lspconfig",
        event = "VeryLazy",
        config = function()
            local lspconfig = require("lspconfig")
            vim.api.nvim_create_autocmd("LspAttach", {
                group = vim.api.nvim_create_augroup('UserLspConfig', {}),
                callback = function(ev)
                    local opts = { buffer = ev.buf, remap = false }
                    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'
                    vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
                    vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
                    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
                    vim.keymap.set("n", "<leader>lr", vim.lsp.buf.references, opts)
                    vim.keymap.set("n", "<leader>sr", vim.lsp.buf.rename, opts)
                    vim.keymap.set("n", "<leader>st", vim.lsp.buf.type_definition, opts)
                    vim.keymap.set("n", "<leader>lf", vim.lsp.buf.format, opts)
                    vim.keymap.set("n", "<C-h>", vim.lsp.buf.signature_help, opts)
                    vim.keymap.set("n", "<C-y>", function() vim.lsp.buf.complete() end, opts)
                end
            })

            local capabilities = vim.lsp.protocol.make_client_capabilities()
            capabilities = vim.tbl_deep_extend('force', capabilities, require('cmp_nvim_lsp').default_capabilities())

            lspconfig.lua_ls.setup({
                --[[
                runtime = { version = 'LuaJIT' },
                workspace = {
                    checkThirdParty = false,
                    library = {
                        '${3rd}/luv/library',
                        unpack(vim.api.nvim_get_runtime_file('', true)),
                    },
                },
                settings = {
                    diagnostics = {
                        disable = { 'missing-fields' }
                    }
                },
                ]]--
                capabilities = capabilities
            })
            lspconfig.rust_analyzer.setup({ capabilities = capabilities })
            lspconfig.terraformls.setup({ capabilities = capabilities })
            lspconfig.pylsp.setup({
                capabilities = capabilities ,
                root_dir = function()
                    return vim.fs.dirname(vim.fs.find({ 'requirements.txt' }, { upward = true })[1])
                end,
            })
            lspconfig.ruff_lsp.setup({
                capabilities = capabilities,
                root_dir = function()
                    return vim.fs.dirname(vim.fs.find({ 'requirements.txt' }, { upward = true })[1])
                end,
            })
            lspconfig.robotframework_ls.setup({
                capabilities = capabilities,
                root_dir = function()
                    return vim.fs.dirname(vim.fs.find({ 'requirements.txt' }, { upward = true })[1])
                end,
                settings = {
                    robot = {
                        python = {
                            executable = '${workspaceFolder}/venv/bin/python3'
                        },
                        language_server = {
                            python = '${workspaceFolder}/venv/bin/python3'
                        },
                        pythonpath = { '${workspaceFolder}', '${workspaceFolder}/Libraries' },
                        libraries = {
                            libdoc = {
                                needsArgs = {"SFClient"}
                            }
                        }
                    }
                }
            })
        end,
    },


    { -- Autocompletion
        'hrsh7th/nvim-cmp',
        event = 'InsertEnter',
        dependencies = {
            -- Snippet Engine & its associated nvim-cmp source
            {
                'L3MON4D3/LuaSnip',
                build = (function()
                    return 'make install_jsregexp'
                end)(),
            },
            'saadparwaiz1/cmp_luasnip',

            -- Adds other completion capabilities.
            'hrsh7th/cmp-nvim-lsp',
            'hrsh7th/cmp-path',

            -- If you want to add a bunch of pre-configured snippets,
            --    you can use this plugin to help you. It even has snippets
            --    for various frameworks/libraries/etc. but you will have to
            --    set up the ones that are useful for you.
            -- 'rafamadriz/friendly-snippets',
        },
        config = function()
            -- See `:help cmp`
            local cmp = require 'cmp'
            local luasnip = require 'luasnip'
            luasnip.config.setup {}

            cmp.setup {
                snippet = {
                    expand = function(args)
                        luasnip.lsp_expand(args.body)
                    end,
                },
                completion = { completeopt = 'menu,menuone,noinsert' },

                -- For an understanding of why these mappings were
                -- chosen, you will need to read `:help ins-completion`
                --
                -- No, but seriously. Please read `:help ins-completion`, it is really good!
                mapping = cmp.mapping.preset.insert {
                    ['<C-n>'] = cmp.mapping.select_next_item(),
                    ['<C-p>'] = cmp.mapping.select_prev_item(),
                    ['<C-y>'] = cmp.mapping.confirm { select = true },
                    ['<C-Space>'] = cmp.mapping.complete {},

                    -- Think of <c-l> as moving to the right of your snippet expansion.
                    --  So if you have a snippet that's like:
                    --  function $name($args)
                    --    $body
                    --  end
                    --
                    -- <c-l> will move you to the right of each of the expansion locations.
                    -- <c-h> is similar, except moving you backwards.
                    ['<C-l>'] = cmp.mapping(function()
                        if luasnip.expand_or_locally_jumpable() then
                            luasnip.expand_or_jump()
                        end
                    end, { 'i', 's' }),
                    ['<C-h>'] = cmp.mapping(function()
                        if luasnip.locally_jumpable(-1) then
                            luasnip.jump(-1)
                        end
                    end, { 'i', 's' }),
                },
                sources = {
                    { name = 'nvim_lsp' },
                    { name = 'luasnip' },
                    { name = 'path' },
                },
            }
        end,
    },

    { -- Autoformat
        'stevearc/conform.nvim',
        opts = {
            notify_on_error = false,
            format_on_save = {
                timeout_ms = 700,
                lsp_fallback = false,
            },
            formatters_by_ft = {
                lua    = { 'stylua' },
                json   = { 'jq' },
                python = { 'ruff_format' },
                rust   = {'rustfmt'},
                -- Conform can also run multiple formatters sequentially
                -- python = { "isort", "black" },
                --
                -- You can use a sub-list to tell conform to run *until* a formatter
                -- is found.
                -- javascript = { { "prettierd", "prettier" } },
            },
            formatters = {
                ruff_format = {
                    cmd = "ruff",
                    prepend_args = { "format", "--line-length=100" }
                }
            }
        },
    },
}
