return {
    {
        "ggandor/leap.nvim",
        event = "VeryLazy",
        dependencies = { "tpope/vim-repeat" },
        config = function()
            vim.keymap.set({ 'n', 'x', 'o' }, 's', '<Plug>(leap-forward)')
            vim.keymap.set({ 'n', 'x', 'o' }, 'S', '<Plug>(leap-backward)')
        end,
    },
    {
        "ggandor/leap-spooky.nvim",
        config = function()
            require("leap-spooky").setup({
                prefix = true
            })
        end,
    }
}
