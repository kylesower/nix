local orange
local colorscheme = vim.api.nvim_command_output("colorscheme")
if colorscheme == "kanagawa" then
    print("kanagawa")
    orange = require('kanagawa.colors').setup().palette.surimiOrange
elseif colorscheme == "solarized-osaka" then
	orange = require("solarized-osaka.colors").setup().orange
elseif colorscheme == "nightfox" then
    orange = require("nightfox.palette").load("nightfox").orange.base
end

local opt = vim.opt
-- Setting the fg cursor color doesn't seem to work, maybe because of Alacritty color precedence.
vim.api.nvim_set_hl(0, 'iCursor', {bg=orange})
opt.guicursor = "i-ci-ve:block-iCursor-blinkwait300-blinkon200-blinkoff150,r-cr-o:hor20"
opt.nu = true
opt.relativenumber = true
opt.tabstop = 4
opt.softtabstop = 4
opt.shiftwidth = 4
opt.expandtab = true
opt.smartindent = true
opt.wrap = true
opt.cursorline = true

opt.swapfile = false
opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
opt.undofile = true

opt.hlsearch = true
opt.incsearch = true
opt.termguicolors = true

opt.scrolloff = 8
opt.signcolumn = "yes"
opt.isfname:append("@-@")
opt.updatetime = 50
opt.colorcolumn = "100"

opt.grepprg = "rg --vimgrep --ignore-case --hidden"

opt.mouse = ""
vim.cmd('au BufNewFile,BufRead *.html set tabstop=2 softtabstop=2 shiftwidth=2')
vim.cmd('au FileType html,javascript,terraform,css set tabstop=2 softtabstop=2 shiftwidth=2')
