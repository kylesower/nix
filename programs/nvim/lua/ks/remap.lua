-- Harpoon
local mark = require("harpoon.mark")
local ui = require("harpoon.ui")
vim.keymap.set("n", "<leader>a", mark.add_file)
vim.keymap.set("n", "<C-l>", ui.toggle_quick_menu)
vim.keymap.set("n", "<leader>1", function() ui.nav_file(1) end)
vim.keymap.set("n", "<leader>2", function() ui.nav_file(2) end)
vim.keymap.set("n", "<leader>3", function() ui.nav_file(3) end)
vim.keymap.set("n", "<leader>4", function() ui.nav_file(4) end)
vim.keymap.set("n", "<leader>5", function() ui.nav_file(5) end)

-- Fugitive
vim.keymap.set("n", "<leader>gs", vim.cmd.Git)
vim.keymap.set("n", "<leader>df", vim.cmd.Gdiffsplit)

-- Telescope
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>pf', builtin.find_files, {})
vim.keymap.set('n', '<C-p>', builtin.git_files, {})
vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})
vim.keymap.set('n', '<leader>b', builtin.buffers, {})
vim.keymap.set('n', '<leader>ps', function()
	builtin.grep_string({ search = vim.fn.input("Grep > ") });
end)

--ZenMode
vim.keymap.set('n', '<leader>z', vim.cmd.ZenMode)


vim.keymap.set("n", "<leader>pv", ":Ex<CR>")
-- Ctrl + arrows to move through windows
vim.keymap.set("n", "<M-Left>", "<C-w>h")
vim.keymap.set("n", "<M-Right>", "<C-w>l")
vim.keymap.set("n", "<M-Up>", "<C-w>k")
vim.keymap.set("n", "<M-Down>", "<C-w>j")

-- Move up and down and center screen in all modes
vim.keymap.set("n", "<Space>", "<C-d>zz")
vim.keymap.set("n", "<Bslash><C-H><Space>", "<C-u>zz")
vim.keymap.set("v", "<Space>", "<C-d>zz")
vim.keymap.set("v", "<Bslash><C-H><Space>", "<C-u>zz")
vim.keymap.set("i", "<Bslash><C-H><Space>", "<Space>")
--vim.keymap.set("i", "<C-u>", "<Space>")
--vim.keymap.set("o", "<Space>", "<C-d>zz")
--vim.keymap.set("o", "<C-u>", "<C-u>zz")

-- Center screen when moving through search
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- Save with C-s
vim.keymap.set("i", "<C-s>", "<C-[>:w<CR>")
vim.keymap.set("n", "<C-s>", ":w<CR>")
vim.keymap.set("v", "<C-s>", ":w<CR>")

-- Save and quit with M-q
vim.keymap.set("i", "<M-q>", "<C-[>:wq<CR>")
vim.keymap.set("n", "<M-q>", ":wq<CR>")
vim.keymap.set("v", "<M-q>", ":wq<CR>")

-- put vim in bg with lx
vim.keymap.set("n", "<leader>x", ":w<CR><C-z>")
vim.keymap.set("v", "<leader>x", ":w<CR><C-z>")

-- Shift lines in visual mode
vim.keymap.set("v", "<S-Down>", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "<S-Up>", ":m '<-2<CR>gv=gv")
-- Shift current line in insert modes
vim.keymap.set("i", "<S-Down>", "<C-[>:m .+1<CR>==gi")
vim.keymap.set("i", "<S-Up>", "<C-[>:m  .-2<CR>==gi")
-- Shift current line in normal mode
vim.keymap.set("n", "<S-Down>", ":m .+1<CR>==")
vim.keymap.set("n", "<S-Up>", ":m .-2<CR>==")

-- Remove search with \
vim.keymap.set("n", "<Bslash>/", ':let @/ = ""<CR>', {desc="Clear search"})

-- Use alt+o/O to add lines without going into insert mode
vim.keymap.set("n", "<M-o>", "o<C-[>k", {desc="Insert line below"})
vim.keymap.set("n", "<M-O>", "O<C-[>j", {desc="Insert line above"})

-- Paste over highlight and retain what was copied in register
vim.keymap.set("x", "<leader>p", "\"_dP")

-- Copy to system clipboard
vim.keymap.set("n", "<leader>y", "\"+y", {desc="Copy to system clipboard"})
vim.keymap.set("v", "<leader>y", "\"+y", {desc="Copy to system clipboard"})
vim.keymap.set("n", "<leader>Y", "\"+Y", {desc="Copy to system clipboard"})

-- Delete to void register
vim.keymap.set("n", "<leader>d", "\"_d")
vim.keymap.set("v", "<leader>d", "\"_d")

-- Global replace of current word
vim.keymap.set("n", "<leader>r", ":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>")

-- Capital U to undo
vim.keymap.set("n", "U", "<C-r>")

vim.keymap.set("n", "<leader>fm", vim.lsp.buf.format)

